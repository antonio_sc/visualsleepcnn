#  Copyright (c) 2020. Antonio Sanjurjo Cortés
"""
This file is part of VisualSleepCNN.

VisualSleepCNN is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualSleepCNN is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualSleepCNN.  If not, see <https://www.gnu.org/licenses/>.
"""

"""
Functions used to map char stages to numbers and the other way around
"""

def stage_num_2_char(num):
    if num == 0:
        return 'W'
    elif num == 1:
        return '1'
    elif num == 2:
        return '2'
    elif num == 3:
        return '3'
    elif num == 4:
        return 'R'
    elif num == 5:
        return '?'
    

def stage_char_2_num(char):
    if char == 'W':
        return 0
    elif char == '1':
        return 1
    elif char == '2':
        return 2
    elif char == '3':
        return 3
    elif char == 'R':
        return 4
    elif char == '?':
        return 5