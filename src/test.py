#  Copyright (c) 2020. Antonio Sanjurjo Cortés
"""
This file is part of VisualSleepCNN.

VisualSleepCNN is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualSleepCNN is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualSleepCNN.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import numpy as np
import cv2
from config import EPOCH_SIZE, SC_PATH, ST_PATH, NET_DATASET, EEG_SUBPATH, EOG_SUBPATH, \
    EMG_SUBPATH, PHYSICAL_LIM, BASE_PATH, TRAIN_TEST_SET, MODELS_BASE_PATH, IMG_WIDTH, IMG_HEIGHT, CV_BASE_PATH, \
    CV_BASE_FOLD_NAME, TRAIN_BATCH_SIZE, CV_K
import stage_mapping as st_map

"""
! Choose one of the backend options. Tensorflow is the default one.
"""
# os.environ["KERAS_BACKEND"] = "plaidml.keras.backend" # For AMD GPU. Last Keras version to support multiple backends is 2.3

from keras.models import load_model
import sklearn
import sklearn.metrics
import argparse
from multiprocessing import Pool
from train import load_net_img

"""
! Use only if needed, ONLY applicable to Tensorflow backend
"""
# from keras import backend as K
# # Limit GPU usage
# config = tf.ConfigProto()
# #config.gpu_options.per_process_gpu_memory_fraction = 0.8
# config.gpu_options.allow_growth=True
# sess = tf.Session(config=config)
# K.set_session(sess)


def test_generator(signals, set_path=TRAIN_TEST_SET, batch_size=32):
    """
    Generator that yields test cases from the indicated signals. This generator can loop forever and shuffles the data
    after every whole dataset loop.

    Args:
        signals: list containing the string representations of the signal names ('eeg' or 'eog').
            !!! Signals MUST be specified in the same order as the net inputs definition
        set_path: test set np.array path
        batch_size: batch size that indicates the number of images loaded and returned on each call

    Returns:
        np.array: test images
        list: sparse representation of the image class (stage)

    """
    # Load test cases paths
    if set_path.endswith('.npz'):
        train_test_set_path = BASE_PATH + '/' + set_path
    else:
        train_test_set_path = BASE_PATH + '/' + set_path + '.npz'
    with np.load(train_test_set_path) as train_test_set:
        eeg_test = train_test_set['eeg_test']
        eog_test = train_test_set['eog_test']
        test_num = len(eeg_test) # Number of test cases
        index = 0
        if (test_num % batch_size != 0):
            last_batch_size = test_num % batch_size
        
        while (1):
            input_list = []
            
            # Last batch of each epoch might be smaller, decide current batch size
            if (index + batch_size) >= test_num:
                curr_batch_size = last_batch_size
            else:
                curr_batch_size = batch_size
            
            # Get test cases
            for signal_index, signal in enumerate(signals):
                input_list.append(np.empty((curr_batch_size, IMG_HEIGHT, IMG_WIDTH, 1), dtype=np.uint8))  # !cv2 order is HEIGHT, WIDTH
                for case in range(0, curr_batch_size):
                    # Read images
                    if signal  == 'eeg': 
                        case_subpath = eeg_test[index+case]
                    elif signal == 'eog':
                        case_subpath = eog_test[index+case]
                    input_img = cv2.imread(BASE_PATH + case_subpath, cv2.IMREAD_GRAYSCALE)
                    input_img = cv2.resize(input_img, (IMG_WIDTH, IMG_HEIGHT))
                    input_list[signal_index][case, :, :] = load_net_img(BASE_PATH + case_subpath)
            
            # Stages are the same in all the signals, only read them once
            expected_list = np.empty(curr_batch_size, dtype=np.uint8)
            for case in range(0, curr_batch_size):
                expected_stage = os.path.splitext(os.path.basename(eeg_test[case]))[0][-1]
                expected_list[case] = st_map.stage_char_2_num(expected_stage)
                
            # Update index in a continuous cyclic way
            index = (index+curr_batch_size) % test_num
            
            # Yield depending on the number of input signals
            if len(signals) == 1:
                # If there's only one signal/input don't return it as a list
                yield (input_list[0], expected_list)
            else:
                yield (input_list, expected_list)


def test_data(signals, set_path=TRAIN_TEST_SET):
    """
    Function that returns all the test cases from the indicated signals.
    !WARNING: If the dataset is big (like the whole Physionet dataset) it will take a lot of RAM.

    Args:
        signals: list containing the string representations of the signal names ('eeg' or 'eog').
            !!! Signals MUST be specified in the same order as the net inputs definition
        set_path: test set np.array path
        batch_size: batch size that indicates the number of images loaded and returned on each call

    Returns:
        np.array: test images
        list: sparse representation of the image class (stage)

    """
    # Load test cases paths
    # Keras will do the shuffle if needed
    if set_path.endswith('.npz'):
        train_test_set_path = BASE_PATH + '/' + set_path
    else:
        train_test_set_path = BASE_PATH + '/' + set_path + '.npz'
    with np.load(train_test_set_path) as train_test_set:
        eeg_test = train_test_set['eeg_test']
        eog_test = train_test_set['eog_test']
        test_num = len(eeg_test) # Number of test cases
        
        input_list = []
        expected_list = []
        
        p = Pool()
        # Get test cases
        for signal_index, signal in enumerate(signals):
            img_paths_list = []
            for case in range(0, test_num):
                # Read images
                if signal  == 'eeg': 
                    case_subpath = eeg_test[case]
                elif signal == 'eog':
                    case_subpath = eog_test[case]

                img_paths_list.append(BASE_PATH + case_subpath)
            input_list.append(np.array(p.map(load_net_img, img_paths_list), dtype=np.uint8))
        
        # Get input class
        # Stages are the same in all the signals, only read them once
        expected_list = np.empty(test_num, dtype=np.uint8)
        for case in range(0, test_num):
            expected_stage = os.path.splitext(os.path.basename(eeg_test[case]))[0][-1]
            expected_list[case] = st_map.stage_char_2_num(expected_stage)

        p.close()

        # Return depending on one or multiple inputs
        if len(signals) == 1:
            # If there's only one signal/input don't return it as a list
            return (input_list[0], expected_list)
        else:
            return (input_list, expected_list)


def execute_test_generator(model, signals, subpath, batch_size=32):
    """
    Execute the test using a generator to load the data

    Args:
        model: Keras compiled model
        signals: string list representing the system input signals (ex: ['eeg'])
        subpath: relative path from BASE_PATH of the testing set
        batch_size: batch size of each testing step (number of images processed during each evaluate call)

    Returns:
         list of scalars: scalars defined during model compilation

    """
    # Load train cases paths and get the number of train cases
    train_test_set_path = os.path.join(BASE_PATH, (subpath + '.npz'))
    with np.load(train_test_set_path) as train_test_set:
        test_length = len(train_test_set['eeg_test'])
        steps = np.ceil(test_length / batch_size)
        results = model.evaluate_generator(test_generator(signals, subpath, batch_size=batch_size), steps=steps, verbose=1, max_queue_size=8)
        
    return results


def execute_test(model, signals, subpath, batch_size=32):
    """
    Execute the test loading the data on memory

    Args:
        model: Keras compiled model
        signals: string list representing the system input signals (ex: ['eeg'])
        subpath: relative path from BASE_PATH of the testing set
        batch_size: batch size of each testing step (number of images processed during each evaluate call)

    Returns:
         list of scalars: scalars defined during model compilation

    """
    test_x, test_y = test_data(signals, subpath)
    results = model.evaluate(test_x, test_y, batch_size=batch_size)
    
    return results


def execute_test_custom_metrics(model, signals, subpath, generator, batch_size=32):
    """
    Execute the testing and calculate metrics like confusion matrix, F1, recall ...

    Args:
        model: Keras compiled model
        signals: string list representing the system input signals (ex: ['eeg'])
        subpath: relative path from BASE_PATH of the testing set
        generator: Bool to indicate if the test data should be loaded using a generator
        batch_size: batch size of each testing step (number of images of each prediction call)

    Returns:
        np.array: NxN sklearn confusion_matrix
        string: text representation of the sklearn classification_report

    """
    # Load train cases paths and get the number of train cases
    train_test_set_path = os.path.join(BASE_PATH, subpath + '.npz')
    with np.load(train_test_set_path) as train_test_set:
        test_length = len(train_test_set['eeg_test'])
        y_true = np.zeros(test_length, dtype=np.uint8)
        # Inference
        if generator:
            steps = np.ceil(test_length / batch_size)
            netout = model.predict_generator(test_generator(signals, batch_size=batch_size), steps=steps, verbose=1,
                                             max_queue_size=8)
        else:
            test_x, _ = test_data(signals, subpath)
            netout = model.predict(test_x, batch_size=batch_size, verbose=1)
        y_pred = np.argmax(netout, axis=1)

        # Get the expected classes (ground truth)
        for idx, case in enumerate(train_test_set['eeg_test']):
            expected_stage = os.path.splitext(os.path.basename(case))[0][-1]
            y_true[idx] = st_map.stage_char_2_num(expected_stage)

        # Extract confusion_matrix
        # print(y_true.shape, y_pred.shape)
        conf_mat = sklearn.metrics.confusion_matrix(y_true, y_pred)

        # Extract classification_report
        labels = ['W', '1', '2', '3', 'R']
        report = sklearn.metrics.classification_report(y_true, y_pred, target_names=labels)

        return conf_mat, report


def get_models_from_path(path):
    """
    Get all the Keras model paths of an specific folder

    Args:
        path: absolute path of the folder

    Returns:
        list: all the Keras models path inside the input folder

    """
    models_path_list = []
    for file in os.listdir(path):
        if file.endswith('.h5') or file.endswith('.hdf5'):
            model_path = os.path.join(path, file)
            print(model_path)
            models_path_list.append(model_path)
            
    return models_path_list


########################################################################################################################
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Test a Keras model. Chose ONLY one signal. It will default to EEG')
    parser.add_argument('model_path', type=str,
                        help='path to a Keras model or folder containing multiple models (relative to the MODELS_BASE_PATH of the project)')
    parser.add_argument('--metrics_mode', default=0, type=int,
                        help='choose the test metrics mode. 0: normal Keras test; 1: complete test with sklearn metrics')
    parser.add_argument('--test_mode', default=0, type=int,
                        help='choose the test mode. 0: normal train&test set style; 1: K-fold cross-validation sets')
    parser.add_argument('--fold_num', default=0, type=int,
                        help='(only if --test_mode is 1) choose cross validation set number to test')
    parser.add_argument('--eeg', action='store_true',
                        help='Perform the testing using the EEG signal')
    parser.add_argument('--eog', action='store_true',
                        help='Perform the testing using the EOG signal')
    parser.add_argument('--generator', action='store_true',
                        help='Use Python generators to perform the testing (use when data doesn\'t fit in memory')

    
    try:
        args = parser.parse_args()
    except argparse.ArgumentError as exc:
        print(exc.message, '\n', exc.argument)

    # Select signals based on user input
    input_signal = []
    if args.eeg is True:
        input_signal.append('eeg')
    elif args.eog is True:
        input_signal.append('eog')
    else:
        input_signal.append('eeg')

    abs_path = os.path.join(MODELS_BASE_PATH, args.model_path)
    batch_size = TRAIN_BATCH_SIZE

    # Check if the input is a folder or a single file
    if os.path.isdir(abs_path):
        paths = get_models_from_path(abs_path)
    else:
        paths = [abs_path]
    
    print("Detected model paths: \n", paths)
    
    for model_path in paths:
        model = load_model(model_path)
        
        print('\n', model_path, '\n')
        model.summary()

        if args.test_mode == 0:  # train&test set
            subpath = TRAIN_TEST_SET
        elif args.test_mode == 1:  # K-fold sets
            if args.fold_num < 0 or args.fold_num >= CV_K:
                print("Incorrect fold number: ", args.fold_num, " must be a number between 0 and ", CV_K-1)
                exit(1)
            subpath = os.path.join(CV_BASE_PATH, (CV_BASE_FOLD_NAME + '_' + str(args.fold_num)))

        if args.metrics_mode == 0:  # normal Keras test
            if args.generator is True:
                test_result = execute_test_generator(model, input_signal, subpath, batch_size=batch_size)
            else:
                test_result = execute_test(model, input_signal, subpath, batch_size=batch_size)
            print(model.metrics_names)
            print(test_result)
        elif args.metrics_mode == 1:  # sklearn metrics
            conf_mat, report = execute_test_custom_metrics(model, input_signal, subpath, generator=args.generator,
                                                           batch_size=batch_size)

            print('W', '1', '2', '3', 'R')
            print(conf_mat)
            print()
            print(report)
        else:
            print('Incorrect --metrics_mode option')
            exit(1)

        print('\n\n')