#  Copyright (c) 2020. Antonio Sanjurjo Cortés

import os

#! All paths should be formed using BASE_PATH + desired_path (using concatenations or joins)
BASE_PATH = os.path.dirname(os.path.dirname(__file__))

# Epoch duration in seconds
EPOCH_SIZE = 30  # Default is 30secs, standard for both R&K and AASM.

# Physical max/min (use positive and negative value in code) of the signals. Used to cut the signal values.
PHYSICAL_LIM = 300

# Channels
EEG_CHANNEL = 0  # Set the channel number of the EEG in the .edf files
EOG_CHANNEL = 2  # Set the channel number of the EOG in the .edf files

MAX_INIT_FINAL_W_TIME = 25  # Defines the maximum amount of Wake stage minutes before and after the sleep. It's used to filter stages.

# EDF files path
SC_PATH = os.path.join("dataset", "sleep-cassette")
ST_PATH = os.path.join("dataset", "sleep-telemetry")

# Network dataset base path
NET_DATASET = os.path.join("dataset", "net_dataset")
EEG_SUBPATH = os.path.join(NET_DATASET, "eeg")
EOG_SUBPATH = os.path.join(NET_DATASET, "eog")
EMG_SUBPATH = os.path.join(NET_DATASET, "emg")

# Train and test set path
TRAIN_TEST_SET = os.path.join("dataset", "train_test_set")
TRAIN_TEST_DIV = 80  # Percentage of train cases

# K-fold cross validation data
CV_BASE_PATH = os.path.join("dataset", "cv_folds")
CV_BASE_FOLD_NAME = "train_test_fold"
CV_K = 5  # The K value of the K-Fold cross validation

MODELS_BASE_PATH = os.path.join(BASE_PATH, 'stored_models')

## Image size
# Main size
IMG_WIDTH = 320
IMG_HEIGHT = 240

# Optional reduced size (faster but less accuracy)
#IMG_WIDTH = 160
#IMG_HEIGHT = 120

# Training config
TRAIN_EPOCHS = 50
TRAIN_BATCH_SIZE = 32
LR = 1e-03  # Learning rate
ADAM_EPSILON = 1e-08  # Adam optimizer