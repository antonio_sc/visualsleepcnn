#  Copyright (c) 2020. Antonio Sanjurjo Cortés
"""
This file is part of VisualSleepCNN.

VisualSleepCNN is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualSleepCNN is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualSleepCNN.  If not, see <https://www.gnu.org/licenses/>.
"""

"""
This module is used to handle the PSG and Hypnograms (get the raw data, stages ...)
"""


import os
import pyedflib
from config import EPOCH_SIZE


def get_edf_readers(path):
    """
    Gets all the edf readers (one for each PSG and one for each Hypnogram) from a given folder

    Args:
        path: PSGs and Hypnograms folder path

    Returns:
        list: list of dictionaries with psg_name, psg_reader and hyp_reader fields.
            Each list element includes one study.

    Files should be closed when not needed anymore with _close()

    """

    # Get all the files and sort them by name
    file_names = sorted(os.listdir(path))

    """
    This supposes that the dataset is the original Physionet Sleep-EDFX one, which
    indexes the PSGs as "n" and their Hypnograms as "n+1"
    """
    edf_list = []
    for i in range(0, len(file_names), 2):
        psg = None
        hyp = None
        try:
            base_name = file_names[i].split('-')[0]  # Get everything before the '-'
            print("psg_name: ", file_names[i], "  hyp_name:", file_names[i + 1])
            psg = pyedflib.EdfReader(os.path.join(path, file_names[i]))
            hyp = pyedflib.EdfReader(os.path.join(path, file_names[i + 1]))
            study = {"psg_name": base_name, "psg_reader": psg, "hyp_reader": hyp}
            edf_list.append(study)
        finally:
            """
            Close the files in order not to saturate the OS open files counter.
            Header data can still be accessed with the file closed, signals can't.
            Use EdfReader.open('file_path') to open them again.
            """
            if psg is not None:
                psg._close()
            if hyp is not None:
                hyp._close()

    return edf_list


def get_epoch_num(edf_reader):
    """
    Calculates the total number of epochs from a given PSG study

    Args:
        edf_reader: the PSG edf reader

    Returns:
        int: number of epochs of that EDF file

    """

    return edf_reader.file_duration // EPOCH_SIZE


def read_signal_epoch(edf_reader, index, epoch):
    """
    Reads the signal indicated by the index from a certain PSG epoch

    Args:
        edf_reader: the PSG edf reader
        index: the signal index in the EDF
        epoch: the epoch number

    Returns:
        list: list containing the signal values

    """

    if get_epoch_num(edf_reader) <= epoch:
        raise Exception("The epoch number is greater than the total number of epochs...")
    else:
        try:
            # Open the file
            edf_reader.open(edf_reader.file_name)

            signal = edf_reader.readSignal(index)
            rate = edf_reader.getSignalHeader(index)['sample_rate']
            # Return a list containing the signal during the given epoch
            epoch_signal = signal[epoch * EPOCH_SIZE * rate: (epoch + 1) * EPOCH_SIZE * rate]

        finally:
            # Close the file
            edf_reader._close()

        return epoch_signal


def read_signal_epochs(edf_reader, index):
    """
    Reads the signal indicated by the index from a certain PSG

    Args:
        edf_reader: the PSG edf reader
        index: the signal index in the EDF

    Returns:
        list: list of lists containing the signal values of that indexed signal. Each sublist contains the values of an epoch

    """

    try:
        # Open the file
        edf_reader.open(edf_reader.file_name)

        num_epochs = get_epoch_num(edf_reader)
        signal = edf_reader.readSignal(index)
        rate = edf_reader.getSignalHeader(index)['sample_rate']

        # Go through all the epochs and append them to the list
        epoch_list = []
        for epoch in range(num_epochs):
            epoch_signal = signal[epoch * EPOCH_SIZE * rate: (epoch + 1) * EPOCH_SIZE * rate]
            epoch_list.append(epoch_signal)

    finally:
        # Close the file
        edf_reader._close()

    return epoch_list


def get_epoch_stage(psg_reader, hyp_reader, epoch):
    """
    Gets the expert assigned stage of the corresponding PSG study for a given input epoch number

    Args:
        psg_reader: the PSG edf reader
        hyp_reader: the Hypnogram edf reader
        epoch: the epoch number

    Returns:
        char: 'W', 'R', '1', '2', '3', '4', 'M' (Movement time) or '?' (not scored)

    In some cases the signal has more epochs that the ones indicated in the Hypnogram, in those cases
    once the function reaches the end of the annotations assigns the  '?' (not scored) to the remaining epochs
    """

    if get_epoch_num(psg_reader) <= epoch:
        raise Exception("The epoch number is greater than the total number of epochs...")
    else:
        try:
            # Open the file
            hyp_reader.open(hyp_reader.file_name)

            epoch_limits = (epoch * EPOCH_SIZE, (epoch + 1) * EPOCH_SIZE - 1)
            # Pyedflib returns annotations as a 3x1 matrix, with the next elements:
            #   [annotation start (s) , annotation duration (s) , annotation description]
            annotations = hyp_reader.readAnnotations()

            # Advance on the annotations until we reach de decision point
            # or we don't have more annotations.
            n = 0
            while (n < (hyp_reader.annotations_in_file - 1)) and (annotations[0][n] < epoch_limits[0]):
                n += 1

            # Get the correct stage annotation
            stage_annotation = "?"
            # Annotation start matches epoch start. Set stage as indicated
            if annotations[0][n] == epoch_limits[0]:
                stage_annotation = annotations[2][n]
            # Last annotation with remaining duration. Set stage as indicated
            elif ((epoch_limits[0] > annotations[0][n]) and
                  (epoch_limits[0] < (annotations[0][n] + annotations[1][n]))):
                stage_annotation = annotations[2][n]
            # Last annotation without remaining duration. (hypnogram finishes before
            # the signal does, thus the stage is unknown)
            elif ((epoch_limits[0] > annotations[0][n]) and
                  (epoch_limits[0] >= (annotations[0][n] + annotations[1][n]))):
                stage_annotation = '?'
            # Annotation start is greater than epoch start. Get previous stage
            elif epoch_limits[0] < (annotations[0][n]):
                stage_annotation = annotations[2][n - 1]

            # Stage annotations are stored as it follows:
            #  "Sleep stage X", where X is one of the described stages
            #  "Movement time", when the patient is moving
            if stage_annotation[0] == "M":
                stage = "M"
            else:
                stage = stage_annotation[-1]

        finally:
            # Close the file
            hyp_reader._close()

        return stage[-1]


def get_stages(psg_reader, hyp_reader):
    """
    Gets the expert assigned stages of the corresponding PSG study

    Args:
        psg_reader: the PSG edf reader
        hyp_reader: the Hypnogram edf reader
        epoch: the epoch number

    Returns:
        char: 'W', 'R', '1', '2', '3', '4', 'M' (Movement time) or '?' (not scored)

    In some cases the signal has more epochs that the ones indicated in the Hypnogram, in those cases
    once the function reaches the end of the annotations assigns the  '?' (not scored) to the remaining epochs
    """

    try:
        # Open the file
        hyp_reader.open(hyp_reader.file_name)

        # Pyedflib returns annotations as a 3x1 matrix, with the next elements:
        #   [annotation start (s) , annotation duration (s) , annotation description]
        annotations = hyp_reader.readAnnotations()
        epochs = get_epoch_num(psg_reader)
        stages = []
        n = 0

        for epoch in range(epochs):
            epoch_limits = (epoch * EPOCH_SIZE, (epoch + 1) * EPOCH_SIZE - 1)

            # Advance on the annotations until we reach de decision point
            # or we don't have more annotations.
            while (n < (hyp_reader.annotations_in_file - 1)) and (annotations[0][n] < epoch_limits[0]):
                n += 1

            # Get the correct stage annotation
            stage_annotation = "?"
            # Annotation start matches epoch start. Set stage as indicated
            if annotations[0][n] == epoch_limits[0]:
                stage_annotation = annotations[2][n]
            # Last annotation with remaining duration. Set stage as indicated
            elif ((epoch_limits[0] > annotations[0][n]) and
                  (epoch_limits[0] < (annotations[0][n] + annotations[1][n]))):
                stage_annotation = annotations[2][n]
            # Last annotation without remaining duration. (hypnogram finishes before
            # the signal does, thus the stage is unknown)
            elif ((epoch_limits[0] > annotations[0][n]) and
                  (epoch_limits[0] >= (annotations[0][n] + annotations[1][n]))):
                stage_annotation = '?'
            # Annotation start is greater than epoch start. Get previous stage
            elif epoch_limits[0] < (annotations[0][n]):
                stage_annotation = annotations[2][n - 1]

            # Stage annotations are stored as follows:
            #  "Sleep stage X", where X is one of the described stages
            #  "Movement time", when the patient is moving
            if stage_annotation[0] == "M":
                stage = "M"
            else:
                stage = stage_annotation[-1]

            stages.append(stage)

    finally:
        # Close the file
        hyp_reader._close()

    return stages


def transform_rk_to_aasm(state):
    """
    Transforms Rechtschaffen & Kales (R&K) annotations to AASM style annotations.
    This is done using the following recomendations:
        https://www.researchgate.net/publication/270911182_Recommendations_for_performance_assessment_of_automatic_sleep_staging_algorithms
    The convention followed is:
        'W' and 'M' -> 'W'
        '1' -> '1'
        '2' -> '2'
        '3' and '4' -> '3'
        'R' -> 'R'
        '?' -> '?'
        Other input -> 'Error'

    Args:
        state: char that indicates the r&k state as defined in the Physionet dataset

    Returns:
        char: an AASM converted stage or 'Error'

    """

    if ('W' == state) or ('M' == state):
        out_state = 'W'
    elif '1' == state:
        out_state = '1'
    elif '2' == state:
        out_state = '2'
    elif ('3' == state) or ('4' == state):
        out_state = '3'
    elif 'R' == state:
        out_state = 'R'
    elif '?' == state:
        out_state = '?'
    else:
        out_state = 'Error'
        print("ERROR: Input state is not valid. "
              "Has to be one of the following: 'W', 'R', '1', '2', '3', '4', 'M' or '?'"
              "and was: ", state)

    return out_state
