#  Copyright (c) 2020. Antonio Sanjurjo Cortés
"""
This file is part of VisualSleepCNN.

VisualSleepCNN is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualSleepCNN is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualSleepCNN.  If not, see <https://www.gnu.org/licenses/>.
"""
import argparse

"""
This module is used to generate the images that form the Neural Network input.
It also handles the creation of train and test sets as well as k-fold cross validation
"""

import glob
import multiprocessing as mp
import os

import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split, KFold

import psg_utils
from config import EPOCH_SIZE, SC_PATH, ST_PATH, NET_DATASET, PHYSICAL_LIM, BASE_PATH, TRAIN_TEST_SET, CV_BASE_PATH, \
    CV_BASE_FOLD_NAME, EEG_CHANNEL, EOG_CHANNEL, MAX_INIT_FINAL_W_TIME, CV_K, TRAIN_TEST_DIV


# TODO add parser or read from config to set the __main__ actions (parser should be better)
# TODO Allow generation of single signal dataset at a time
# TODO Allow generation of single signal train/test set or fold

class Dataset:
    IMG_FORMAT = 'png'  # File format of the exported images

    def __init__(self, paths):
        """
        Initializes the stats and gets the EDF readers

        Args:
            paths: list of paths containing EDFs (PSGs and Hypnograms)
        """
        
        if len(paths) == 0:
            raise Exception("Paths cannot be empty...")
        else:
            # Create list with the files of all the given paths
            edf_readers = []
            for path in paths:
                edf_readers.extend(psg_utils.get_edf_readers(path))
                
            self.edf_readers = edf_readers
            
        # Create dictionary to contain stats
        self.stats = {"W": 0, "R": 0, "1": 0, "2": 0, "3": 0, "Discarded": 0}


    def get_stats(self):
        """
        Calculates the general stats of the dataset. This is, the number of cases of each AASM stage

        Returns:
            dictionary: The following dataset stat values -> {"W": int, "R": int, "1": int, "2": int, "3": int, "Discarded": int}

        """

        for value in self.stats.values():
            if value > 0:
                return self.stats.copy()            
        
        self.get_dataset(export_imgs=False)
        return self.stats.copy()


    def export_stats(self):
        """
        Saves the current stats to a .txt file
        """

        stats = self.get_stats()
        with open(os.path.join(BASE_PATH, "dataset", "dataset_stats.txt"), 'w') as f:
            for key, val in stats.items():
                f.write(key + ":\t" + str(val) + "\n")


    def get_dataset(self, export_imgs=True):
        """
        Go through all the dataset files, calculate the stats and export the images if indicated.
        For each file :
            - Get necessary signals (EEG, EOG, EMG)
            - Get R&K state annotations
            - Filter invalid stage states : '?' stage
            - Convert R&K to AASM
            - Filter the signals based on a Wake time limit
            - (Generate signal images (net input))
            - (Save images)

        Args:
            export_imgs: Bool to indicate if it should export the signal images

        """

        # Make sure stats are reinitialized (in case of multiple executions)
        self.stats = {"W": 0, "R": 0, "1": 0, "2": 0, "3": 0, "Discarded": 0}

        for study in self.edf_readers:
            psg_name = study.get("psg_name")
            print("\nReading: ", psg_name, " cases")

            if export_imgs is True:
                # Create a pool for each study and wait (join) until a study is exported to move to the next one. This helps
                # synchronization and to keep a reasonable amount of RAM usage although it is slower
                pool = mp.Pool()  # Creates a pool with as many workers as CPU threads
                out_base_path = os.path.join(BASE_PATH, NET_DATASET)
                eeg_export_path = os.path.join(out_base_path, psg_name, "eeg")
                eog_export_path = os.path.join(out_base_path, psg_name, "eog")
                os.makedirs(eeg_export_path)
                os.makedirs(eog_export_path)
            
            # Get necessary signals
            psg_reader = study.get("psg_reader")
            eeg = psg_utils.read_signal_epochs(psg_reader, EEG_CHANNEL)
            eog = psg_utils.read_signal_epochs(psg_reader, EOG_CHANNEL)

            # Get hypnogram annotations
            hyp_reader = study.get("hyp_reader")
            stages = psg_utils.get_stages(psg_reader, hyp_reader)
            
            # Convert R&K to AASM
            aasm_stages = []
            for stage in stages:
                out_stage = psg_utils.transform_rk_to_aasm(stage)
                aasm_stages.append(out_stage)
            
            # Filter some of the Wake stages (the most common stage)
            max_time = MAX_INIT_FINAL_W_TIME  # Max number of minutes since include before and after the sleep
            no_wake_idx = np.where(np.array(aasm_stages) != "W")[0]
            first_idx = int(np.ceil(no_wake_idx[0] - (max_time * (60 / EPOCH_SIZE))))
            last_idx = int(np.ceil(no_wake_idx[-1] + (max_time * (60 / EPOCH_SIZE))))
            # Make sure indexes contain valid values
            if first_idx < 0:
                first_idx = 0
            if last_idx >= len(aasm_stages):
                last_idx = len(aasm_stages) - 1
            selected_idx = np.arange(first_idx, last_idx)
            print("Epochs before filtering Wake stages: ", len(aasm_stages))
            print("Epochs after filtering Wake stages: ", len(selected_idx))

            for epoch in selected_idx:
                stage = aasm_stages[epoch]
                # Filter invalids and register stats
                if ('?' == stage):
                    self.stats.update({"Discarded": self.stats.get("Discarded") + 1})
                    continue
                self.stats.update({stage: self.stats.get(stage) + 1})
                
                # Export signal images
                if export_imgs is True:
                    pool.starmap_async(gen_image, [(eeg[epoch], eeg_export_path, str(epoch) + "_" + stage),
                                                   (eog[epoch], eog_export_path, str(epoch) + "_" + stage)])
            # Join after a whole study is analyzed in order to limit the RAM usage
            if export_imgs is True:
                pool.close()
                pool.join()


    def gen_sets(self, out_subpath=TRAIN_TEST_SET, train_per=TRAIN_TEST_DIV):
        """
        Generates a random train and test set using a given train % and exports it to a numpy array file.
        Each array will contain paths relative to BASE_PATH, making it machine independent.

        Args:
            out_subpath: output subpath (relative to the BASE_PATH) of the train and test sets
            train_per: training percentage of the total amount of cases

        """
        
        # Get all the dataset cases relative paths
        net_dataset = os.path.join(BASE_PATH, NET_DATASET)
        eeg_files = glob.glob(net_dataset + '/**/eeg/*.' + Dataset.IMG_FORMAT, recursive=True)
        eog_files = glob.glob(net_dataset + '/**/eog/*.' + Dataset.IMG_FORMAT, recursive=True)

        # Get paths relative to the BASE_PATH (remove the BASE_PATH from the glob output)
        eeg_files = list(map(lambda x: x.split(BASE_PATH)[1], eeg_files))
        eog_files = list(map(lambda x: x.split(BASE_PATH)[1], eog_files))

        try:
            assert len(eeg_files) == len(eog_files)
        except:
            print("\nNumber of EEG or EOG files doesn't match. Regenerate the dataset or manually fix it")
            eeg_files = [s.replace('eeg', '') for s in eeg_files]
            eog_files = [s.replace('eog', '') for s in eog_files]
            print("\nShowing differences:\n")
            print(set(eeg_files).symmetric_difference(set(eog_files)))
            return

        # Create a list with all the indexes
        files_num_list = list(range(len(eeg_files)))
        
        # Create the split sets
        train_set, test_set = train_test_split(files_num_list, train_size=(train_per/100))
        
        # Create train case lists
        eeg_train = []
        eog_train = []
        for x in train_set:
            eeg_train.append(eeg_files[x])
            eog_train.append(eog_files[x])
            
        # Create test case lists
        eeg_test = []
        eog_test = []
        for x in test_set:
            eeg_test.append(eeg_files[x])
            eog_test.append(eog_files[x])
        
        # Convert to arrays
        eeg_train = np.array(eeg_train)
        eog_train = np.array(eog_train)
        eeg_test = np.array(eeg_test)
        eog_test = np.array(eog_test)
        
        # Save npz file
        np.savez(os.path.join(BASE_PATH, out_subpath), eeg_train=eeg_train,
                 eog_train=eog_train, eeg_test=eeg_test, eog_test=eog_test)


    def gen_folds(self, out_subpath=CV_BASE_PATH, n_folds=CV_K):
        """
        Generates random train and test splits using the K-fold cross validation method and exports it as numpy array.
        Each array will contain paths relative to BASE_PATH, making it machine independent.

        Args:
            out_subpath: output subpath (relative to the BASE_PATH) for the cross validation
            n_folds: number of folds (K) to use for the K-fold

        """

        # Get all the dataset cases relative paths
        net_dataset = os.path.join(BASE_PATH, NET_DATASET)
        eeg_files = glob.glob(net_dataset + '/**/eeg/*.' + Dataset.IMG_FORMAT, recursive=True)
        eog_files = glob.glob(net_dataset + '/**/eog/*.' + Dataset.IMG_FORMAT, recursive=True)

        # Get paths relative to the BASE_PATH (remove the BASE_PATH from the glob output)
        eeg_files = list(map(lambda x: x.split(BASE_PATH)[1], eeg_files))
        eog_files = list(map(lambda x: x.split(BASE_PATH)[1], eog_files))

        try:
            assert len(eeg_files) == len(eog_files)
        except:
            print("\nNumber of EEG or EOG files doesn't match. Regenerate the dataset or manually fix it")
            eeg_files = [s.replace('eeg', '') for s in eeg_files]
            eog_files = [s.replace('eog', '') for s in eog_files]
            print("\nShowing differences:\n")
            print(set(eeg_files).symmetric_difference(set(eog_files)))
            return
        
        export_base_path = os.path.join(BASE_PATH, out_subpath)
        # Create a list with all the indexes
        files_num_list = list(range(len(eeg_files)))
        
        # Create folds
        kf = KFold(n_folds, shuffle=True)
        
        i = 0
        if not os.path.isdir(export_base_path):
            os.makedirs(export_base_path)
        for train_index, test_index in kf.split(files_num_list):
             # Create train case lists
            eeg_train = []
            eog_train = []
            for x in train_index:
                eeg_train.append(eeg_files[x])
                eog_train.append(eog_files[x])
                
            # Create test case lists
            eeg_test = []
            eog_test = []
            for x in test_index:
                eeg_test.append(eeg_files[x])
                eog_test.append(eog_files[x])
                
            # Convert to arrays
            eeg_train = np.array(eeg_train)
            eog_train = np.array(eog_train)
            eeg_test = np.array(eeg_test)
            eog_test = np.array(eog_test)
            
            # Save npz file
            np.savez(os.path.join(export_base_path, CV_BASE_FOLD_NAME + '_' + str(i)), eeg_train=eeg_train,
                 eog_train=eog_train, eeg_test=eeg_test, eog_test=eog_test)
            i += 1


def gen_image(signal, path, filename, file_format=Dataset.IMG_FORMAT):
    """
    Generates an image that contains the given signal epoch. Stores it in the given path with the given file_format.

    Args:
        signal: one of the PSG signals (typically a whole epoch)
        path: export path of the image
        filename: name of the saved image
        file_format: image file format

    """

    fig = plt.figure()
    plt.plot(signal, color='black')
    plt.xticks([])
    plt.yticks([]) # Remove spaces before and after signal
    plt.axis('off')
    plt.axis([0, EPOCH_SIZE*100, -PHYSICAL_LIM, PHYSICAL_LIM], linewidth=1)
    # To eliminate white spaces surrounding the signal, generated by matplotlib use:
    plt.tight_layout() # alternatively you can use bbox_inches='tight' parameter in savefig
    plt.savefig(os.path.join(path, filename + '.' + file_format), format=file_format, dpi=100)
    plt.close(fig)


########################################################################################################################
if __name__ == "__main__":
    # Add as many input paths as needed
    st_path = os.path.join(BASE_PATH, ST_PATH)
    sc_path = os.path.join(BASE_PATH, SC_PATH)
    dataset = Dataset([st_path, sc_path])

    parser = argparse.ArgumentParser(description='Generate the image dataset, get stats or export train&test sets or K-fold cross validation sets.')
    parser.add_argument('--export', action='store_true',
                        help='Generate the image dataset')

    # TODO add options to export only one signal. train.py and test.py have to be adapted too as they now suppose both signals are present
    """
    parser.add_argument('--eeg', action='store_true',
                        help='Only applicable when using --export, --train_sets or --cv. Use EEG signal')
    parser.add_argument('--eog', action='store_true',
                        help='Only applicable when using --export, --train_sets or --cv. Use EOG signal')
    """
    parser.add_argument('--stats', action='store_true',
                        help='Export dataset stats')
    parser.add_argument('--train_sets', action='store_true',
                        help='Export dataset train and test sets')
    parser.add_argument('--cv', action='store_true',
                        help='Export dataset K-fold cross validation sets')

    try:
        args = parser.parse_args()
    except argparse.ArgumentError as exc:
        print(exc.message, '\n', exc.argument)
        exit(1)

    if args.export is True:
        dataset.get_dataset()
    elif args.stats is True:
        dataset.export_stats()
        print(dataset.get_stats())
    elif args.train_sets is True:
        dataset.gen_sets()
    elif args.cv is True:
        dataset.gen_folds()
    else:
        print("No input options provided, execute with -h to see the available options")