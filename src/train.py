#  Copyright (c) 2020. Antonio Sanjurjo Cortés
"""
This file is part of VisualSleepCNN.

VisualSleepCNN is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualSleepCNN is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualSleepCNN.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import numpy as np
import cv2

from config import BASE_PATH, TRAIN_TEST_SET, MODELS_BASE_PATH, IMG_WIDTH, IMG_HEIGHT, CV_BASE_PATH, TRAIN_EPOCHS, \
    TRAIN_BATCH_SIZE, \
    LR, ADAM_EPSILON
import stage_mapping as st_map

"""
! Choose one of the backend options. Tensorflow is the default one.
"""

# TODO add eeg+eog models and corresponding execution options

# os.environ["KERAS_BACKEND"] = "plaidml.keras.backend"  # For AMD GPU. Last Keras version to support multiple backends is 2.3

from keras import metrics, losses
from keras.layers import Dense, Flatten, Dropout, BatchNormalization, MaxPooling2D, Input, Activation
from keras.models import Model
from keras.layers.convolutional import Conv2D
from keras.optimizers import Adam
from keras.utils import plot_model
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau
import matplotlib.pyplot as plt
import sklearn
from datetime import datetime
import glob
import argparse
from multiprocessing import Pool

"""
! Use only if needed, ONLY applicable to Tensorflow backend
"""


# # Limit GPU usage
# from keras import backend as K
# config = tf.ConfigProto()
# #config.gpu_options.per_process_gpu_memory_fraction = 0.8
# config.gpu_options.allow_growth=True
# sess = tf.Session(config=config)
# K.set_session(sess)


def load_net_img(img_path):
    """
    Receives an image paths, loads the image, reescales it and returns it
    Args:
        img_path: image path

    Returns:
        np.array: array containing the image in grayscale

    """

    input_img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
    input_img = cv2.resize(input_img, (IMG_WIDTH, IMG_HEIGHT))  # Reads values from the config file
    return input_img.reshape((IMG_HEIGHT, IMG_WIDTH, 1))  # !cv2 order is HEIGHT, WIDTH


def train_generator(signals, set_path=TRAIN_TEST_SET, batch_size=32):
    """
    Generator that yields train cases from the indicated signals. This generator can loop forever and shuffles the data
    after every whole dataset loop.

    Args:
        signals: list containing the string representations of the signal names ('eeg' or 'eog').
            !!! Signals MUST be specified in the same order as the net inputs definition
        set_path: training set np.array path
        batch_size: batch size that indicates the number of images loaded and returned on each call

    Returns:
        np.array: train images
        list: sparse representation of the image class (stage)

    """
    # Load train cases paths
    if set_path.endswith('.npz'):
        train_test_set_path = BASE_PATH + '/' + set_path
    else:
        train_test_set_path = BASE_PATH + '/' + set_path + '.npz'
    with np.load(train_test_set_path) as train_test_set:
        # np.load(train_test_set_path)
        eeg_train = train_test_set['eeg_train']
        eog_train = train_test_set['eog_train']
        train_num = len(eeg_train)
        index = 0
        if (train_num % batch_size != 0):
            last_batch_size = train_num % batch_size

        while (1):
            input_list = []
            expected_list = []

            # Last batch of each epoch might be smaller, decide current batch size
            if (index + batch_size) >= train_num:
                curr_batch_size = last_batch_size
            else:
                curr_batch_size = batch_size

            # Shuffle train cases every epoch
            if (index == 0):
                eeg_train, eog_train = sklearn.utils.shuffle(eeg_train, eog_train)

            # Get train cases
            for signal_index, signal in enumerate(signals):
                input_list.append(np.empty((curr_batch_size, IMG_HEIGHT, IMG_WIDTH, 1),
                                           dtype=np.uint8))  # !cv2 order is HEIGHT, WIDTH
                for case in range(0, curr_batch_size):
                    # Read images
                    if signal == 'eeg':
                        case_subpath = eeg_train[index + case]
                    elif signal == 'eog':
                        case_subpath = eog_train[index + case]
                    input_list[signal_index][case, :, :] = load_net_img(BASE_PATH + case_subpath)

            # Stages are the same in all the signals, only read them once
            expected_list = np.empty(curr_batch_size, dtype=np.uint8)
            for case in range(curr_batch_size):
                expected_stage = os.path.splitext(os.path.basename(eeg_train[case]))[0][-1]
                expected_list[case] = st_map.stage_char_2_num(expected_stage)

            # Update index in a continuous cyclic way
            index = (index + curr_batch_size) % train_num

            # Yield depending on the number of input signals
            if len(signals) == 1:
                # If there's only one signal/input don't return it as a list
                yield (input_list[0], expected_list)
            else:
                yield (input_list, expected_list)


def train_data(signals, set_path=TRAIN_TEST_SET):
    """
    Function that returns all the train cases from the indicated signals.
    !WARNING: If the dataset is big (like the whole Physionet dataset) it will take a lot of RAM (ex: with 320x240 images
    it will use more than 18GB of RAM).

    Args:
        signals: list containing the string representations of the signal names ('eeg' or 'eog').
            !!! Signals MUST be specified in the same order as the net inputs definition
        set_path: training set np.array path
        batch_size: batch size that indicates the number of images loaded and returned on each call

    Returns:
        np.array: train images
        list: sparse representation of the image class (stage)

    """
    # Load train cases paths
    # ! Keras will do the shuffle if needed
    if set_path.endswith('.npz'):
        train_test_set_path = BASE_PATH + '/' + set_path
    else:
        train_test_set_path = BASE_PATH + '/' + set_path + '.npz'
    with np.load(train_test_set_path) as train_test_set:
        eeg_train = train_test_set['eeg_train']
        eog_train = train_test_set['eog_train']
        train_num = len(eeg_train)
        input_list = []

        p = Pool()
        # Get train cases
        for signal_index, signal in enumerate(signals):
            img_paths_list = []
            for case in range(0, train_num):
                # Read images
                if signal == 'eeg':
                    case_subpath = eeg_train[case]
                elif signal == 'eog':
                    case_subpath = eog_train[case]
                img_paths_list.append(BASE_PATH + case_subpath)
            input_list.append(np.array(p.map(load_net_img, img_paths_list), dtype=np.uint8))

        # Get input class
        # Stages are the same in all the signals, only read them once
        expected_list = np.empty(train_num, dtype=np.uint8)
        for case in range(0, train_num):
            expected_stage = os.path.splitext(os.path.basename(eeg_train[case]))[0][-1]
            expected_list[case] = st_map.stage_char_2_num(expected_stage)
        p.close()

        # Return depending on one or multiple inputs
        if len(signals) == 1:
            # If there's only one signal/input don't return it as a list
            return (input_list[0], expected_list)
        else:
            return (input_list, expected_list)


################################################ NEURAL NETWORK MODELS #################################################
def model_7():
    input_shape = (IMG_HEIGHT, IMG_WIDTH, 1)
    input_img = Input(shape=input_shape)

    # CNN part
    # Note: 'same' padding uses zero padding. 'valid' only uses valid values (doesn't add padding)
    x = Conv2D(16, (9, 9), padding='same', strides=(1, 1), use_bias=False, name='conv1')(input_img)
    x = BatchNormalization(name='batch1')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(2, 2), name='pool1')(x)

    x = Conv2D(24, (7, 7), padding='same', strides=(1, 1), use_bias=False, name='conv2')(x)
    x = BatchNormalization(name='batch2')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(2, 2), name='pool2')(x)

    x = Conv2D(32, (3, 3), padding='same', strides=(1, 1), use_bias=False, name='conv3')(x)
    x = BatchNormalization(name='batch3')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(2, 2), name='pool3')(x)

    # FC part
    x = Flatten()(x)
    x = Dense(256, use_bias=False, name='fc1')(x)
    x = BatchNormalization(name='batch5')(x)
    x = Activation('relu')(x)

    x = Dropout(0.5)(x)

    x = Dense(256, use_bias=False, name='fc2')(x)
    x = BatchNormalization(name='batch6')(x)
    x = Activation('relu')(x)

    x = Dropout(0.5)(x)

    x = Dense(5, name='fc3')(x)
    # Softmax to obtain class probabilities
    x = Activation('softmax')(x)

    output = x

    return Model(inputs=input_img, outputs=output)


def model_9():
    input_shape = (IMG_HEIGHT, IMG_WIDTH, 1)
    input_img = Input(shape=input_shape)

    # CNN part
    # Note: 'same' padding uses zero padding. 'valid' only uses valid values (doesn't add padding)
    x = Conv2D(12, (5, 5), padding='same', strides=(1, 1), use_bias=False, name='conv1')(input_img)
    x = BatchNormalization(name='batch1')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(4, 4), strides=(2, 2), name='pool1')(x)

    x = Conv2D(16, (5, 5), padding='same', strides=(1, 1), use_bias=False, name='conv2')(x)
    x = BatchNormalization(name='batch2')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(4, 4), strides=(2, 2), name='pool2')(x)

    x = Conv2D(24, (3, 3), padding='same', strides=(1, 1), use_bias=False, name='conv3')(x)
    x = BatchNormalization(name='batch3')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(4, 4), strides=(2, 2), name='pool3')(x)

    x = Conv2D(32, (3, 3), padding='same', strides=(1, 1), use_bias=False, name='conv4')(x)
    x = BatchNormalization(name='batch4')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(4, 4), strides=(2, 2), name='pool4')(x)

    # FC part
    x = Flatten()(x)
    x = Dense(256, use_bias=False, name='fc1')(x)
    x = BatchNormalization(name='batch5')(x)
    x = Activation('relu')(x)

    x = Dropout(0.5)(x)

    x = Dense(256, use_bias=False, name='fc2')(x)
    x = BatchNormalization(name='batch6')(x)
    x = Activation('relu')(x)

    x = Dropout(0.5)(x)

    x = Dense(5, name='fc3')(x)
    # Softmax to obtain class probabilities
    x = Activation('softmax')(x)

    output = x

    return Model(inputs=input_img, outputs=output)


def model_13():
    # Based on model_7 but with dropout in the convs and only one FC
    input_shape = (IMG_HEIGHT, IMG_WIDTH, 1)
    input_img = Input(shape=input_shape)

    # CNN part
    # Note: 'same' padding uses zero padding. 'valid' only uses valid values (doesn't add padding)
    x = Conv2D(16, (9, 9), padding='same', strides=(1, 1), use_bias=False, name='conv1')(input_img)
    x = BatchNormalization(name='batch1')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(2, 2), name='pool1')(x)
    x = Dropout(0.25)(x)

    x = Conv2D(32, (7, 7), padding='same', strides=(1, 1), use_bias=False, name='conv2')(x)
    x = BatchNormalization(name='batch2')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(2, 2), name='pool2')(x)
    x = Dropout(0.25)(x)

    x = Conv2D(64, (3, 3), padding='same', strides=(1, 1), use_bias=False, name='conv3')(x)
    x = BatchNormalization(name='batch3')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(2, 2), name='pool3')(x)
    x = Dropout(0.25)(x)

    # FC part
    x = Flatten()(x)
    x = Dense(256, use_bias=False, name='fc1')(x)
    x = BatchNormalization(name='batch5')(x)
    x = Activation('relu')(x)

    x = Dropout(0.5)(x)

    x = Dense(5, name='fc2')(x)
    # Softmax to obtain class probabilities
    x = Activation('softmax')(x)

    output = x

    return Model(inputs=input_img, outputs=output)


############################################ END NEURAL NETWORK MODELS #################################################

def execute_training_generator(model, signals, epochs=50, batch_size=32, callbacks=None):
    """
    Execute the model training using a generator to load the input data.

    Args:
        model: Keras compiled model
        signals: string list representing the system input signals (ex: ['eeg'])
        epochs: total number of epochs (understanding it as a whole train set loop)
        batch_size: batch size of each training step (number of images of each fit call)
        callbacks: list of Keras fit callbacks

    Returns:
        Keras.History: The training history with the calculated metrics

    """
    # Load train cases paths and get the number of train cases
    train_test_set_path = os.path.join(BASE_PATH, (TRAIN_TEST_SET + '.npz'))

    with np.load(train_test_set_path) as train_test_set:
        train_length = len(train_test_set['eeg_train'])
        steps = np.ceil(train_length / batch_size)
        history = model.fit_generator(train_generator(signals=signals, batch_size=batch_size), epochs=epochs,
                                      steps_per_epoch=steps, max_queue_size=8, callbacks=callbacks)

    return history


def execute_training_memory(model, signals, epochs=50, batch_size=32, callbacks=None):
    """
    Execute the model training using loading all the data on memory.
    !WARNING: It can take a lot of RAM, which could end up with a system lockup or with the OS killing the program

    Args:
        model: Keras compiled model
        signals: string list representing the system input signals (ex: ['eeg'])
        epochs: total number of epochs (understanding it as a whole train set loop)
        batch_size: batch size of each training step (number of images of each fit call)
        callbacks: list of Keras fit callbacks

    Returns:
        Keras.History: The training history with the calculated metrics

    """
    train_x, train_y = train_data(signals=signals)
    return model.fit(train_x, train_y, epochs=epochs, batch_size=batch_size, shuffle=True, callbacks=callbacks)


def create_export_dir(base_dir='./'):
    """
    Creates a directory to store the training models and other related files like graphics.
    The function uses the current date and time to generate a new folder.

    Args:
        base_dir: the parent directory from which it will create the new directory

    Returns:
        string: The new directory path

    """
    today = datetime.now()
    created_dir_path = os.path.join(
        base_dir + '/' + today.strftime('%d_%m_%Y') + '__' + str(today.hour) + '_' + str(today.minute))
    os.makedirs(created_dir_path)

    return created_dir_path


def export_training_graphics(history, export_path):
    """
    Export training graphics for the accuracy and loss

    Args:
        history: a Keras.History object
        export_path: the path used to store the graphics

    """
    # Training accuracy
    plt.plot(history.history['sparse_categorical_accuracy'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train'], loc='upper left')
    fig_name = os.path.join(export_path, 'accuracy.png')
    plt.savefig(fig_name, format='png', dpi=120)

    # Training loss
    plt.plot(history.history['loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train'], loc='upper left')
    fig_name = os.path.join(export_path, 'loss.png')
    plt.savefig(fig_name, format='png', dpi=120)


def get_checkpoint_callback(checkpoint_path, save_best_only=True):
    """
    Generates a Keras checkpoint callback to store model checkpoints with a custom name

    Args:
        checkpoint_path: path used to store the model checkpoints
        save_best_only: used to optimize space, it will only save a new checkpoint if the monitored metric is better than
            in the previous evaluation

    Returns:
        Keras.callback: ModelCheckpoint callback

    """
    checkpoint_model_path = os.path.join(checkpoint_path,
                                         'weights.{epoch:02d}--{sparse_categorical_accuracy:.4f}.hdf5')
    return ModelCheckpoint(checkpoint_model_path, monitor='sparse_categorical_accuracy', period=1,
                           save_best_only=save_best_only)


########################################################################################################################
if __name__ == "__main__":
    valid_model_names = ['model_7', 'model_9', 'model_13']  # Fill as necessary and update parser doc

    parser = argparse.ArgumentParser(
        description='Train a Keras model and save it. Chose ONLY one signal. It will default to EEG.'
                    'The default model is model_9.')
    parser.add_argument('--mode', default=0, type=int,
                        help='Choose the train mode. 0 for normal training and 1 for cross-validation training')
    parser.add_argument('--eeg', action='store_true',
                        help='Perform the training using the EEG signal')
    parser.add_argument('--eog', action='store_true',
                        help='Perform the training using the EOG signal')
    parser.add_argument('--generator', action='store_true',
                        help='Use Python generators to perform the training (use when data doesn\'t fit in memory')
    parser.add_argument('--save_checkpoints', action='store_true',
                        help='Saves training checkpoints')
    parser.add_argument('--model_name', type=str, default='model_9',
                        help='Choose the function name of the model you want to train. Valid ones are: '
                             'model_7, model_9, model_13')
    parser.add_argument('--plot_model', action='store_true',
                        help='Saves the model plot')
    parser.add_argument('--lr_decay', action='store_true',
                        help='apply a learning decay. Adam shoudn\'t be very affected by this')

    try:
        args = parser.parse_args()
    except argparse.ArgumentError as exc:
        print(exc.message, '\n', exc.argument)
        exit(1)

    if args.model_name not in valid_model_names:
        print("Wrong model name. Input model name is: ", args.model_name, " and has to be one of the following: ",
              valid_model_names)
        exit(1)
    model_function = locals()[args.model_name]

    # Select signals based on user input
    input_signal = []
    if args.eeg is True:
        input_signal.append('eeg')
    elif args.eog is True:
        input_signal.append('eog')
    else:
        input_signal.append('eeg')

    epochs = TRAIN_EPOCHS
    batch_size = TRAIN_BATCH_SIZE
    lr = LR
    epsilon = ADAM_EPSILON
    model_export_path = create_export_dir(MODELS_BASE_PATH)

    if args.lr_decay is True:
        lr_decay_cb = ReduceLROnPlateau(monitor='loss', factor=0.75, patience=5, verbose=1, mode='auto', min_lr=1e-05)

    if args.mode == 1:  # Cross-validation training
        fold_files_paths = sorted(glob.glob(os.path.join(BASE_PATH, CV_BASE_PATH) + '*/*'))
        fold_files_paths = list(map(lambda x: x.split(BASE_PATH)[1], fold_files_paths))
        for idx, file_path in enumerate(fold_files_paths):
            model = model_function()
            # Note: targets are integers, use sparse_categorical_* instead of categorical_*
            model.compile(optimizer=Adam(lr=lr, epsilon=epsilon), loss=losses.sparse_categorical_crossentropy,
                          metrics=[metrics.mae, metrics.sparse_categorical_accuracy])
            if idx == 0: model.summary()
            callbacks = []
            if args.lr_decay is True:
                callbacks.append(lr_decay_cb)
            current_fold_path = os.path.join(model_export_path, str(idx))
            os.mkdir(current_fold_path)
            if args.save_checkpoints is True:
                callbacks.append(get_checkpoint_callback(current_fold_path))
            if args.generator is True:
                hist = execute_training_generator(model, input_signal, epochs, batch_size, callbacks=callbacks)
            else:
                hist = execute_training_memory(model, input_signal, epochs, batch_size, callbacks=callbacks)
            # Save the final model
            model.save(os.path.join(current_fold_path, 'model.h5'))
            export_training_graphics(hist, current_fold_path)

    else:  # Normal training (train & test set)
        model = model_function()
        model.summary()
        # Note: targets are integers, use sparse_categorical_* instead of categorical_*
        model.compile(optimizer=Adam(lr=lr, epsilon=epsilon), loss=losses.sparse_categorical_crossentropy,
                      metrics=[metrics.sparse_categorical_accuracy])
        callbacks = []
        if args.lr_decay is True:
            callbacks.append(lr_decay_cb)
        if args.save_checkpoints is True:
            callbacks.append(get_checkpoint_callback(model_export_path))
        if args.generator is True:
            hist = execute_training_generator(model, input_signal, epochs, batch_size, callbacks=callbacks)
        else:
            hist = execute_training_memory(model, input_signal, epochs, batch_size, callbacks=callbacks)
        # Save the final model
        model.save(os.path.join(model_export_path, 'model.h5'))
        export_training_graphics(hist, model_export_path)

    # Print training metrics
    print(hist.history.keys())
    if plot_model is True:
        plot_model(model, to_file=model_export_path + '/model.png', show_shapes=True)