# VisualSleepCNN  
  
**Copyright**: Antonio Sanjurjo Cortés    
    
**E-mail**: antonio.sanjurjoc@udc.es    
    
**Licence**: This project is licenced under GLP-v3. LICENCE.txt contains a copy.    
   
   
Table of contents
=================
  * [Project objective](#project objective)
  * [Structure](#structure)
  * [Usage](#usage)
  * [Software requirements ](#software-requirements )


## Project objective
This project is based on a end-of-degree project in Computer Science. The main objective is to implement a Deep Learning Neural Network that receives as input images representing typical Polysomnography signals (electrophysiological signals) like EEG and EOG.  
  
The used dataset is extracted from Physionet(https://www.physionet.org/content/sleep-edfx/1.0.0/). All the studies except:  
ST7021, ST7071, ST7092, ST7131, ST7132, ST7141, ST7142 are used. This ones are discarded as they have a different file heading
in the Hypnograms and PyEDFlib doesn't recognize them as valid. They can be used to perform final testing with a complete study,
just generate the images and open the hypnogram with a reading tool (online, datasheet program like LibreOffice Calc ...).


## Structure    
#### dataset folder (create locally)    
dataset folder content is not uploaded due to it's size. It's designed to hold the original PSG and Hypnogram files as well as the generated dataset (images) and train and test sets.  
  
#### src folder  
code folder  
  
#### stored_models (create locally)
The content is not uploaded due to it's size. It's designed to be used to save the trained models  
  
#### LICENCE.txt  
A copy of the used Software licence  


## Usage  
 1. Clone the repository and create the auxiliary folders (stored_models and dataset)
 2. Place the input PSG and Hypnograms inside the dataset folder
		 The default placement for the Physionet dataset is:
    > dataset/sleep-cassette
    >
    > dataset/sleep-telemetry
		 
 3. Generate the image dataset (only during the first execution or if you want to modify the images) -> **dataset.py**
 4. Perform the CNN training -> **train.py**
 5. Test the CNN model -> **test.py**

The previously mentioned Python scripts can be executed with different options. 
To see the available options execute the scripts with the *"-h"* flag. ex:
    ```
    python train.py -h
    ```
or open the desired script and check the possibilities at the beginning of the
    ```
    if __name__ == "__main__":
    ```


## Software requirements    
This code is written in Python3 has been tested with the following packages and versions:    
  
It has been tested with the following versions:  
 - **Python** 3.6.9  
 - **h5py** 2.10.0  !!! <3.0.0 Otherwise Keras.load_model will fail (already exported models won't work unless you modify the function that fails)
 - **pyyaml** 5.3.1
 - **Keras** 2.2.4  
 - **Keras-Applications** 1.0.8  
 - **Keras-Preprocessing** 1.1.2  
 - **matplotlib** 3.1.3  
 - **numpy** 1.19.1  
 - **pyEDFlib** 0.1.17  
 - **tensorboard** 1.14.0  
 - **tensorflow** 1.14.0  
 - **tensorflow-estimator** 1.14.0  
 
 *For saliency.py*
 - **keras-vis** 0.4.1
 - **scipy** 1.2.0  ! keras-vis doesn't work properly with scipy>=1.3. (https://github.com/raghakot/keras-vis/issues/209)
  
It can also be used with **AMD GPUs** using ROCm for Tensorflow or PlaidML. The **PlaidML** version used has been:  
 - plaidbench 0.6.4  
 - plaidml 0.6.4  
 - plaidml-keras 0.6.4  
In order to use PlaidML uncomment the   
`os.environ["KERAS_BACKEND"] = "plaidml.keras.backend"`  
line at the beginning of the train.py and test.py files.  
The maximum Keras version for PlaidML is 2.3, as next Keras versions only support the Tensorflow backend.  
  
The best way to execute is the project is to create a new Python environment or use tools like Anaconda(https://www.anaconda.com/products/individual)